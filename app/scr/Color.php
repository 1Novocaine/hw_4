<?php

namespace App\scr;

/**
 * Class Color
 * @package App\scr
 */
class Color
{
    /**
     * @var int|mixed
     */
    private int $red = 0;
    /**
     * @var int|mixed
     */
    private int $green = 0;
    /**
     * @var int|mixed
     */
    private int $blue = 0;

    /**
     * Color constructor.
     * @param int $red
     * @param int $green
     * @param int $blue
     */
    public function __construct($red=0, $green=0, $blue=0)
    {
        $this->red = self::fixRgbValue($red);
        $this->green = self::fixRgbValue($green);
        $this->blue = self::fixRgbValue($blue);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected static function fixRgbValue($value)
    {
        return max(min(round((int)$value), 255), 0);
    }

    /**
     * @param $color1
     * @param $color2
     * @return Color
     */
    public static function mixColors ($color1, $color2)
    {
        return new Color(($color1->red + $color2->red)/2, ($color1->green + $color2->green)/2, ($color1->blue + $color2->blue)/2);
    }

    /**
     * randomise incoming values
     */
    public function randomColor()
    {
        $this->red = rand(0, 255);
        $this->green = rand(0, 255);
        $this->blue = rand(0, 255);
    }

    /**
     * @param $red
     * @param $green
     * @param $blue
     * @return string
     */
    public function fromRGB($red, $green, $blue)
    {
        $red = dechex($red);
        if (strlen($red)<2)
            $red = '0'.$red;

        $green = dechex($green);
        if (strlen($green)<2)
            $green = '0'.$green;

        $blue = dechex($blue);
        if (strlen($blue)<2)
            $blue = '0'.$blue;

        return '#' . $red . $green . $blue;
    }

    /**
     * @return string
     */
    public function getRgbColorValues ()
    {
        return $rgbColor = implode(',', [$this->getRed(), $this->getGreen(), $this->getBlue()]);
    }

    /**
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @param int $red
     */
    public function setRed(int $red): void
    {
        $this->red = $red;
    }

    /**
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @param int $green
     */
    public function setGreen(int $green): void
    {
        $this->green = $green;
    }

    /**
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @param int $blue
     */
    public function setBlue(int $blue): void
    {
        $this->blue = $blue;
    }


}

