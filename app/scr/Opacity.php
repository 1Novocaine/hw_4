<?php

namespace App\scr;

/**
 * Class Opacity
 * @package App\scr
 */
class Opacity
{
    /**
     * @var float|mixed
     */
    private float $opacity;

    /**
     * Opacity constructor.
     * @param float $opacity
     */
    public function __construct($opacity = 0.0)
    {
        $this->opacity = self::fixOpacityValue($opacity);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected static function fixOpacityValue($value)
    {
        return max(min((float)$value, 0.0), 1.0);
    }

    /**
     * randomise incoming values
     */
    public function randomOpacity()
    {
       $this->opacity = rand(0, 10)/10;
    }

    /**
     * @return float|mixed
     */
    public function getOpacity()
    {
        return $this->opacity;
    }
}