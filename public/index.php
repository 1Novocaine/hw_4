<?php


require_once '../vendor/autoload.php';


$randomColor = new \App\scr\Color();
$randomColor->randomColor();
$randomOpacity = new \App\scr\Opacity();
$bodyColor = new \App\scr\Color();
$bodyColor->randomColor();
$color1 = new \App\scr\Color(11, 22,2);
$color2 = new \App\scr\Color(144,122,133);
$rgbColor = $color2->getRgbColorValues();
$mixedColor = \App\scr\Color::mixColors($color1, $color2 );
$randomOpacity->randomOpacity();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="...">
    <title>Random Colors</title>
</head>

<body style="background: rgba( <?php echo $bodyColor->getRgbColorValues() . ',' . $randomOpacity->getOpacity();?>)">

<div class="container">
    <a href="index.php" class="btn">Refresh</a>
    <div class="row">

            <div class="item" style="background-color: rgba(<?php echo $rgbColor;?>, 1)">
                <h1><?php echo $color2->fromRGB($color2->getRed(),$color2->getGreen(),$color2->getBlue()); ?></h1>
            </div>
            <div class="item" style="background:  rgb(<?php echo $randomColor->getRgbColorValues();?>)">
                <h1><?php echo ' Красный: ' . $randomColor->getRed() .  '; Зеленый: ' . $randomColor->getGreen() . '; Синий: ' . $randomColor->getBlue(); ?> </h1>
            </div>
            <div class="item" style="background:  rgb(<?php echo $mixedColor->getRgbColorValues();?>)">
                <h1><?php echo ' Красный: ' . $mixedColor->getRed() .  '; Зеленый: ' . $mixedColor->getGreen() . '; Синий: ' . $mixedColor->getBlue(); ?> </h1>
            </div>
    </div>
</div>

</body>

</html>

